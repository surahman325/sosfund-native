import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:socialfund/animation/fade_animation.dart';
import 'package:socialfund/login/login_page.dart';
import 'package:syncfusion_flutter_charts/charts.dart';


class HomeDesa extends StatefulWidget {
  @override
  _HomeDesaState createState() => _HomeDesaState();
}

class _HomeDesaState extends State<HomeDesa> {
  int _cIndex = 0;

  void _incrementTab(index) {
    setState(() {
      _cIndex = index;
    });
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => keluar(),
      child: Scaffold(
        bottomNavigationBar: Theme(
          data: Theme.of(context).copyWith(
            canvasColor: Colors.white
          ),
          child: BottomNavigationBar(
            currentIndex: _cIndex,
            type: BottomNavigationBarType.shifting,
            items: [
              BottomNavigationBarItem(
                icon: _cIndex == 0 ? Image.asset("assets/gambar/home-run.png", scale: 2,) : Image.asset("assets/gambar/home.png", scale: 2,),
                title: new Text('', style: TextStyle(color: Colors.black),)
              ),
              BottomNavigationBarItem(
                icon: _cIndex == 1 ? Image.asset("assets/gambar/import-run.png", scale: 2,) : Image.asset("assets/gambar/import.png", scale: 2,),
                title: new Text('')
              ),
              BottomNavigationBarItem(
                icon: _cIndex == 2 ? Image.asset("assets/gambar/print-run.png", scale: 2,) : Image.asset("assets/gambar/print.png", scale: 2,),
                title: new Text('')
              ),
              BottomNavigationBarItem(
                icon: _cIndex == 3 ? Image.asset("assets/gambar/staff-run.png", scale: 2,) : Image.asset("assets/gambar/staff.png", scale: 2,),
                title: new Text('')
              )
            ],
            onTap: (index){
                _incrementTab(index);
            },
          ),
        ),
        body: Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Container(
                color: Color(0xFF6EBDFC),
                child: Column(
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height * 0.12,
                      color: Color(0xFF6EBDFC),
                      child: Padding(
                        padding: EdgeInsets.only(left:15, right: 15, top: MediaQuery.of(context).size.height * 0.03),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            CircleAvatar(
                              radius: MediaQuery.of(context).size.width * 0.07,
                              backgroundColor: Colors.white,
                              child: CircleAvatar(
                                radius: 25,
                                backgroundImage: AssetImage('assets/gambar/logo.png'),
                                backgroundColor: Colors.white,
                              ),
                            ),
                            AutoSizeText("Social Fund Transfer", style: TextStyle(color: Colors.white, fontSize: MediaQuery.of(context).size.width * 0.06),),
                            Icon(Icons.notifications, color: Colors.white,size:MediaQuery.of(context).size.width * 0.08,)
                          ],
                        ),
                      ),
                    ),
                    Container(
                      color: Color(0xFF6EBDFC),
                      height: MediaQuery.of(context).size.height * 0.15,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              CircleAvatar(
                                radius: MediaQuery.of(context).size.width * 0.1,
                                backgroundColor: Colors.white,
                                // backgroundImage: AssetImage("assets/gambar/sugihmukti.png"),
                                child: Image.asset("assets/gambar/sugihmukti.png", scale: 4,),
                              ),
                              SizedBox(width: 15,),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text('Cianjur', style: TextStyle(color: Colors.white, fontSize: MediaQuery.of(context).size.width * 0.06),),
                                  Text('Desa Nagrak', style: TextStyle(color: Colors.white, fontSize: MediaQuery.of(context).size.width * 0.05),)
                                ],
                              ),
                            ],
                          ),
                          SizedBox(child: Container(width: 3,height: MediaQuery.of(context).size.height * 0.1,color: Colors.white,),),
                          OutlineButton(
                            child: new Text("Edit Profil", style: TextStyle(color: Colors.white,),),
                            onPressed: (){},
                            borderSide: BorderSide(color: Colors.white),
                            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0))
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.64,
                child: ListView(
                  padding: EdgeInsets.zero,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: MediaQuery.of(context).size.height * 0.12,
                              width: MediaQuery.of(context).size.width * 0.4,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    blurRadius: 10.0,
                                    spreadRadius: 1.0,
                                    offset: Offset(
                                      0.0,
                                      5.0,
                                    ),
                                  )
                                ],
                              ),
                              child: Padding(
                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, top: MediaQuery.of(context).size.height * 0.01, bottom: MediaQuery.of(context).size.height * 0.01),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Image.asset("assets/gambar/spreadsheet.png",scale: 5,),
                                    AutoSizeText("Import Excel", maxLines: 1, minFontSize: 18, maxFontSize: 20,),
                                    AutoSizeText("Input data melalui Excel", maxLines: 1, minFontSize: 12, maxFontSize: 15,),
                                  ],
                                ),
                              ),
                            ),
                            GestureDetector(
                              onTap: (){
                              },
                              child: Container(
                                height: MediaQuery.of(context).size.height * 0.12,
                                width: MediaQuery.of(context).size.width * 0.4,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      blurRadius: 10.0,
                                      spreadRadius: 1.0,
                                      offset: Offset(
                                        0.0,
                                        5.0,
                                      ),
                                    )
                                  ],
                                ),
                                child: Padding(
                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, top: MediaQuery.of(context).size.height * 0.01, bottom: MediaQuery.of(context).size.height * 0.01),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children: <Widget>[
                                      Image.asset("assets/gambar/research.png",scale: 5,),
                                      AutoSizeText("Hasil Data", maxLines: 1, minFontSize: 18, maxFontSize: 20,),
                                      AutoSizeText("Daftar penerima dana", maxLines: 1, minFontSize: 12, maxFontSize: 15,),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: MediaQuery.of(context).size.height * 0.12,
                              width: MediaQuery.of(context).size.width * 0.4,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    blurRadius: 10.0,
                                    spreadRadius: 1.0,
                                    offset: Offset(
                                      0.0,
                                      5.0,
                                    ),
                                  )
                                ],
                              ),
                              child: Padding(
                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, top: MediaQuery.of(context).size.height * 0.01, bottom: MediaQuery.of(context).size.height * 0.01),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Image.asset("assets/gambar/printer.png",scale: 5,),
                                    AutoSizeText("Cetak Data", maxLines: 1, minFontSize: 18, maxFontSize: 20,),
                                    AutoSizeText("Cetak data penerima dana", maxLines: 1, minFontSize: 12, maxFontSize: 15,),
                                  ],
                                ),
                              ),
                            ),
                            GestureDetector(
                              onTap: (){
                              },
                              child: Container(
                                height: MediaQuery.of(context).size.height * 0.12,
                                width: MediaQuery.of(context).size.width * 0.4,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      blurRadius: 10.0,
                                      spreadRadius: 1.0,
                                      offset: Offset(
                                        0.0,
                                        5.0,
                                      ),
                                    )
                                  ],
                                ),
                                child: Padding(
                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, top: MediaQuery.of(context).size.height * 0.01, bottom: MediaQuery.of(context).size.height * 0.01),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children: <Widget>[
                                      Image.asset("assets/gambar/gallery.png",scale: 5,),
                                      AutoSizeText("Foto Penerimaan", maxLines: 1, minFontSize: 18, maxFontSize: 20,),
                                      AutoSizeText("Input bukti pencairan", maxLines: 1, minFontSize: 12, maxFontSize: 15,),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: MediaQuery.of(context).size.height * 0.12,
                              width: MediaQuery.of(context).size.width * 0.4,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    blurRadius: 10.0,
                                    spreadRadius: 1.0,
                                    offset: Offset(
                                      0.0,
                                      5.0,
                                    ),
                                  )
                                ],
                              ),
                              child: Padding(
                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, top: MediaQuery.of(context).size.height * 0.01, bottom: MediaQuery.of(context).size.height * 0.01),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Image.asset("assets/gambar/operator.png",scale: 5,),
                                    AutoSizeText("Kelola Operator", maxLines: 1, minFontSize: 18, maxFontSize: 20,),
                                    AutoSizeText("Tambah, Edit, atau Hapus", maxLines: 1, minFontSize: 12, maxFontSize: 15,),
                                  ],
                                ),
                              ),
                            ),
                            GestureDetector(
                              onTap: (){
                                logout();
                              },
                              child: Container(
                                height: MediaQuery.of(context).size.height * 0.12,
                                width: MediaQuery.of(context).size.width * 0.4,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      blurRadius: 10.0,
                                      spreadRadius: 1.0,
                                      offset: Offset(
                                        0.0,
                                        5.0,
                                      ),
                                    )
                                  ],
                                ),
                                child: Padding(
                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, top: MediaQuery.of(context).size.height * 0.01, bottom: MediaQuery.of(context).size.height * 0.01),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children: <Widget>[
                                      Image.asset("assets/gambar/logout.png",scale: 5,),
                                      AutoSizeText("Logout", maxLines: 1, minFontSize: 18, maxFontSize: 20,),
                                      AutoSizeText("Keluar Dari Aplikasi", maxLines: 1, minFontSize: 12, maxFontSize: 15,),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                        Container(
                          height: MediaQuery.of(context).size.height * 0.2,
                          width: MediaQuery.of(context).size.width * 0.87,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                blurRadius: 10.0,
                                spreadRadius: 1.0,
                                offset: Offset(
                                  0.0,
                                  5.0,
                                ),
                              )
                            ],
                          ),
                          child: Padding(
                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, top: MediaQuery.of(context).size.height * 0.01, bottom: MediaQuery.of(context).size.height * 0.01),
                            child: SfCartesianChart(
                              primaryXAxis: CategoryAxis(),
                              series: <LineSeries<SalesData, String>>[
                                LineSeries<SalesData, String>(
                                  dataSource:  <SalesData>[
                                    SalesData('Jan', 35),
                                    SalesData('Feb', 28),
                                    SalesData('Mar', 34),
                                    SalesData('Apr', 32),
                                    SalesData('May', 40)
                                  ],
                                  xValueMapper: (SalesData sales, _) => sales.year,
                                  yValueMapper: (SalesData sales, _) => sales.sales
                                )
                              ]
                            ),
                          ),
                        ),
                        SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void logout(){
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LoginPage()));
  }

  Future<bool> keluar(){
    return showDialog(
      context: context,
      builder: (context) => FadeAnimation(
        0.5,
        new AlertDialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          title: new Text('Konfirmasi'),
          content: new Text('Yakin ingin keluar aplikasi?'),
          actions: <Widget>[
            FadeAnimation(
              1,
              new FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(18.0),
                  side: BorderSide(color: Colors.red)),
                onPressed: () => Navigator.of(context).pop(false),
                child: Text("Tidak", style: TextStyle(color: Colors.red),),
              ),
            ),
            SizedBox(height: 16),
            FadeAnimation(
              1.5,
              new FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(18.0),
                  side: BorderSide(color: Colors.green)),
                onPressed: () => SystemChannels.platform.invokeMethod('SystemNavigator.pop'),
                child: Text("Ya", style: TextStyle(color: Colors.green),),
              ),
            ),
          ],
        ),
      ),
    ) ??
    false;
  }
}

class SalesData {
  SalesData(this.year, this.sales);
  final String year;
  final double sales;
}