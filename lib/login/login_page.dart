import 'dart:convert';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:socialfund/animation/fade_animation.dart';
import 'package:socialfund/bjb/pilih_kecamatan.dart';
import 'package:http/http.dart' as http;
import 'package:socialfund/login/ubah_password.dart';

import '../api.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String _email, _password;
  bool lihatPassword = false;
  bool loading = false;
  String untukValidEmail = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void initState(){
    super.initState();
    SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
    ]);
  }

  // @override
  // dispose(){
  //   SystemChrome.setPreferredOrientations([
  //     DeviceOrientation.landscapeRight,
  //     DeviceOrientation.landscapeLeft,
  //     DeviceOrientation.portraitUp,
  //     DeviceOrientation.portraitDown,
  //   ]);
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop:  () => keluar(),
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        body: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Container(
            color: Colors.white,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Form(
              key: _formKey,
              child: Stack(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * 0.5,
                    decoration: BoxDecoration(
                      color: Colors.transparent,
                      image: DecorationImage(
                        image: AssetImage("assets/gambar/bg.png"),
                        fit: BoxFit.cover,
                      ),
                    )
                  ),
                  Positioned(
                    top: MediaQuery.of(context).size.height * 0.25,
                    child: Padding(
                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05, right: MediaQuery.of(context).size.width * 0.05),
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.9,
                        height: MediaQuery.of(context).size.height * 0.5,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              blurRadius: 10.0,
                              spreadRadius: 1.0,
                              offset: Offset(
                                0.0,
                                5.0,
                              ),
                            )
                          ],
                          borderRadius: BorderRadius.circular(10)
                        ),
                        child: Column(
                          children: <Widget>[
                            SizedBox(height: MediaQuery.of(context).size.height * 0.12,),
                            AutoSizeText("Login",
                              style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.w500, color: Color(0xFF707070)),
                              maxLines: 1,
                            ),
                            SizedBox(height: MediaQuery.of(context).size.height * 0.05,),
                            Container(
                              width:  MediaQuery.of(context).size.width * 0.75,
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    height: MediaQuery.of(context).size.width * 0.12,
                                    child: TextFormField(
                                      validator: (input) {
                                        Pattern pattern = untukValidEmail;
                                        RegExp regex = new RegExp(pattern);
                                        if (!regex.hasMatch(input)){
                                          return 'Masukkan Email yang valid';
                                        }else{
                                          return null;
                                        }
                                      },
                                      keyboardType: TextInputType.emailAddress,
                                      onSaved: (input) => _email = input,
                                      style: TextStyle(color: Color(0xFF707070)),
                                      decoration: InputDecoration(
                                        labelText: 'Email',
                                        labelStyle: TextStyle(color: Color(0xFF707070), fontSize: 20),
                                        fillColor: Color(0xFF707070),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: Color(0xFF707070)),
                                          borderRadius: BorderRadius.circular(10),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: Color(0xFF707070)),
                                          borderRadius: BorderRadius.circular(10),
                                        ),
                                        errorBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: Color(0xFF707070)),
                                          borderRadius: BorderRadius.circular(10),
                                        ),
                                        focusedErrorBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: Color(0xFF707070)),
                                          borderRadius: BorderRadius.circular(10),
                                        ),
                                        hintText: "masukkan email anda",
                                        hintStyle: TextStyle(color: Color(0xFF707070), fontSize: 12.0)
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: MediaQuery.of(context).size.height * 0.02,),
                                  Container(
                                    height:  MediaQuery.of(context).size.width * 0.12,
                                    child: TextFormField(
                                      validator: (input) {
                                        if (input.length < 6)
                                          return 'harus lebih 6 karakter';
                                        else
                                          return null;
                                      },
                                      onSaved: (input) => _password = input,
                                      obscureText: !lihatPassword,
                                      style: TextStyle(color: Color(0xFF707070)),
                                      decoration: InputDecoration(
                                        labelText: "Password",
                                        labelStyle: TextStyle(color: Color(0xFF707070), fontSize: 20),
                                        fillColor: Color(0xFF707070),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: Color(0xFF707070)),
                                          borderRadius: BorderRadius.circular(10),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: Color(0xFF707070)),
                                          borderRadius: BorderRadius.circular(10),
                                        ),
                                        errorBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: Color(0xFF707070)),
                                          borderRadius: BorderRadius.circular(10),
                                        ),
                                        focusedErrorBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: Color(0xFF707070)),
                                          borderRadius: BorderRadius.circular(10),
                                        ),
                                        suffixIcon: lihatPassword ? GestureDetector(
                                          onTap: (){
                                            setState(() {
                                              lihatPassword = !lihatPassword;
                                            });
                                          },
                                          child: Icon(Icons.visibility, color: Color(0xFF707070),)
                                        ) : GestureDetector(
                                          onTap: (){
                                            setState(() {
                                              lihatPassword = !lihatPassword;
                                            });
                                          },
                                          child: Icon(Icons.visibility_off, color: Color(0xFF707070),)
                                        ),
                                        hintText: "Masukkan Password anda",
                                        hintStyle: TextStyle(color: Color(0xFF707070), fontSize: 12.0)),
                                    ),
                                  ),
                                  SizedBox(height: MediaQuery.of(context).size.height * 0.02,),
                                  InkWell(
                                    child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      height:  MediaQuery.of(context).size.width * 0.12,
                                      decoration: BoxDecoration(
                                          color: Color(0xFF30ABE5),
                                          borderRadius: BorderRadius.circular(10),
                                      ),
                                      child: Material(
                                        color: Colors.transparent,
                                        child: InkWell(
                                          onTap: () {
                                            login();
                                          },
                                          child: Center(
                                            child: !loading ? Text("Masuk",
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontFamily: "Poppins-Bold",
                                                fontSize: 20,
                                                letterSpacing: 1.0
                                              )
                                            ) : CircularProgressIndicator(
                                              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ),
                                  // SizedBox(height: MediaQuery.of(context).size.height * 0.02,),
                                  // InkWell(
                                  //   onTap: () {
                                      
                                  //   },
                                  //   child: AutoSizeText("Lupa password?",
                                  //     style: TextStyle(color: Color(0xFF707070),
                                  //     ),
                                  //     maxLines: 1, minFontSize: 12, maxFontSize: 20
                                  //   )
                                  // )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ),
                  Positioned(
                    top: MediaQuery.of(context).size.height * 0.2,
                    left: MediaQuery.of(context).size.width * 0.2,
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.6,
                      height: MediaQuery.of(context).size.height * 0.12,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            blurRadius: 10.0,
                            spreadRadius: 1.0,
                            offset: Offset(
                              0.0,
                              5.0,
                            ),
                          )
                        ],
                        borderRadius: BorderRadius.circular(20)
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Image.asset(
                          "assets/gambar/logoSFT.png",
                        ),
                      ),
                    )
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void login() async {
    setState(() {
      loading = true;
    });
    final formState = _formKey.currentState;
    formState.save();
    String link = linkApi();
    var data = {'username':_email,'password':_password};
          
    final response = await http.post(link+'login', body: data);
    if (response.statusCode == 200 || response.statusCode == 201) {
      final prefs = await SharedPreferences.getInstance();
      final key = 'infoUser';
      final value = response.body;
      prefs.setString(key, value);
      final dataUser = jsonDecode(prefs.getString(key)) ?? 0;
      // print('read: '+dataUser['id'].toString());
      
      if (dataUser['role'] == "superadmin") {
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => PilihKecamatan()));
      } else if(dataUser['controller'] != null){
        prefs.setString('userpass', jsonEncode(data));
        final userPass = jsonDecode(prefs.getString('userpass')) ?? 0;
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => UbahPassword(username: userPass['username'], controller: dataUser['controller'],)));
      } else {
        showDialog(
          context: context,
          builder: (context) => FadeAnimation(
            0.5,
            new AlertDialog(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
              title: new Text('Info'),
              content: new Text('Username atau Password salah, silahkan cek kembali'),
              actions: <Widget>[
                FadeAnimation(
                  1,
                  FlatButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(18.0),
                      side: BorderSide(color: Colors.green)),
                    onPressed: () => Navigator.of(context).pop(false),
                    child: Text("Ok", style: TextStyle(color: Colors.green),),
                  ),
                ),
              ],
            ),
          ),
        );
      }

      setState(() {
        loading = false;
      });
      // else if (dataUser['role'] == "admin") {
      //   Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeKecamatan()));
      // } else if (dataUser['role'] == "desa") {
      //   Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeDesa()));
      // }
    }else{
      print(response.body);
      showDialog(
        context: context,
        builder: (context) => FadeAnimation(
          0.5,
          new AlertDialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
            title: new Text('Info'),
            content: new Text('Terjadi kesalahan pada server'),
            actions: <Widget>[
              FadeAnimation(
                1,
                FlatButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(18.0),
                    side: BorderSide(color: Colors.green)),
                  onPressed: () => Navigator.of(context).pop(false),
                  child: Text("Ok", style: TextStyle(color: Colors.green),),
                ),
              ),
            ],
          ),
        ),
      );
      setState(() {
        loading = false;
      });
    }
  }

  Future<bool> keluar(){
    return showDialog(
      context: context,
      builder: (context) => FadeAnimation(
        0.5,
        new AlertDialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          title: new Text('Konfirmasi'),
          content: new Text('Yakin ingin keluar aplikasi?'),
          actions: <Widget>[
            FadeAnimation(
              1,
              new FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(18.0),
                  side: BorderSide(color: Colors.red)),
                onPressed: () => Navigator.of(context).pop(false),
                child: Text("Tidak", style: TextStyle(color: Colors.red),),
              ),
            ),
            SizedBox(height: 16),
            FadeAnimation(
              1.5,
              new FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(18.0),
                  side: BorderSide(color: Colors.green)),
                onPressed: () => SystemChannels.platform.invokeMethod('SystemNavigator.pop'),
                child: Text("Ya", style: TextStyle(color: Colors.green),),
              ),
            ),
          ],
        ),
      ),
    ) ??
    false;
  }

}
