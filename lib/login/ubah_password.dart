import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:socialfund/animation/fade_animation.dart';
import 'package:socialfund/login/login_page.dart';
import '../api.dart';

class UbahPassword extends StatefulWidget {
  final username;
  final controller;
  UbahPassword({Key key, @required this.username, this.controller}) : super(key: key);
  @override
  _UbahPasswordState createState() => _UbahPasswordState();
}

class _UbahPasswordState extends State<UbahPassword> {
  bool loading = false;
  String _hp, _password;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          title: Text('Ganti Password'),
          backgroundColor: Color(0xFF1F91F3),
          centerTitle: true,
          leading: Icon(Icons.vpn_key),
        ),
        body: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Container(
            color: Colors.white,
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  SizedBox(height: MediaQuery.of(context).size.height * 0.02,),
                  Container(
                    color: Colors.transparent,
                    height: MediaQuery.of(context).size.height / 4,
                    child: Center(child: Image.asset('assets/gambar/fpw.png'))
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.05,),
                  AutoSizeText("Silahkan Lengkapi Form Di Bawah Untuk Melanjutkan", maxLines: 1,minFontSize: 15,),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.05,),
                  Container(
                    height:  MediaQuery.of(context).size.width * 0.12,
                    width: MediaQuery.of(context).size.width * 0.8,
                    child: TextFormField(
                      onSaved: (input) => _password = input,
                      style: TextStyle(color: Color(0xFF707070)),
                      decoration: InputDecoration(
                        labelText: "Password Baru",
                        labelStyle: TextStyle(color: Color(0xFF707070), fontSize: 20),
                        fillColor: Color(0xFF707070),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xFF707070)),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xFF707070)),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        hintText: "Masukkan Password Baru",
                        hintStyle: TextStyle(color: Color(0xFF707070), fontSize: 12.0)),
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.02,),
                  Container(
                    height:  MediaQuery.of(context).size.width * 0.12,
                    width: MediaQuery.of(context).size.width * 0.8,
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      onSaved: (input) => _hp = input,
                      style: TextStyle(color: Color(0xFF707070)),
                      decoration: InputDecoration(
                        labelText: "No WhatsApp",
                        labelStyle: TextStyle(color: Color(0xFF707070), fontSize: 20),
                        fillColor: Color(0xFF707070),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xFF707070)),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xFF707070)),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        hintText: "Masukkan No WhatsApp",
                        hintStyle: TextStyle(color: Color(0xFF707070), fontSize: 12.0)),
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.05,),
                  InkWell(
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.8,
                      height:  MediaQuery.of(context).size.width * 0.12,
                      decoration: BoxDecoration(
                          color: Color(0xFF30ABE5),
                          borderRadius: BorderRadius.circular(10),
                      ),
                      child: Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () {
                            gantiPassword();
                          },
                          child: Center(
                            child: !loading ? Text("Lanjutkan",
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: "Poppins-Bold",
                                fontSize: 20,
                                letterSpacing: 1.0
                              )
                            ) : CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                            ),
                          ),
                        ),
                      ),
                    )
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void gantiPassword() async {
    setState(() {
      loading = true;
    });
    final formState = _formKey.currentState;
    formState.save();
    String link = linkApi();
    var data = {'hp':_hp,'password':_password, 'username':widget.username};

    final response = await http.post(link+widget.controller, body: data);
    if (response.statusCode == 200 || response.statusCode == 201) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => FadeAnimation(
          0.5,
          new AlertDialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
            title: new Text('Info'),
            content: new Text('Ganti Password berhasil, silahkan login kembali'),
            actions: <Widget>[
              FadeAnimation(
                1,
                new FlatButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(18.0),
                    side: BorderSide(color: Colors.green)),
                  onPressed: () => Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LoginPage())),
                  child: Text("Ok", style: TextStyle(color: Colors.green),),
                ),
              ),
            ],
          ),
        ),
      );
      setState(() {
         loading = false;
      });
    }else{
      print(response.body);
      setState(() {
         loading = false;
      });
    }
  }
}