import 'package:flutter/material.dart';
import 'login/login_page.dart';
import 'package:syncfusion_flutter_core/core.dart';

// void main() => runApp(Main());
void main() { 
  SyncfusionLicense.registerLicense("NT8mJyc2IWhia31ifWN9Z2FoZXxhfGFjYWNzYmFpYmJpZmNzAx5oICYhMjs+Mj1gYWYTND4yOj99MDw+"); 
  return runApp(Main()); 
}

class Main extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Absensi',
      theme: ThemeData(
        primaryColor: Color(0xFF6094E3),
        bottomSheetTheme: BottomSheetThemeData(backgroundColor: Colors.transparent),
      ),
      home: LoginPage(),
    );
  }
}
