import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:socialfund/animation/fade_animation.dart';
import 'package:socialfund/bjb/pilih_kecamatan.dart';
import 'package:socialfund/login/login_page.dart';
import '../api.dart';
import 'package:qrscan/qrscan.dart' as scanner;


class TellerPage extends StatefulWidget {
  @override
  _TellerPageState createState() => _TellerPageState();
}

class _TellerPageState extends State<TellerPage> {
  bool loading = true;
  bool loadKec = true;
  bool loadingCair = false;
  bool loadingScan = false;
  var kecamatan;
  var idDesaAktif;

  void ambildataShared() async {
    final prefs = await SharedPreferences.getInstance();
    final idKec = prefs.getInt("id_kecamatan") ?? 0;
    final idDesa = prefs.getInt("id_desa") ?? 0;
    idDesaAktif = idDesa;
    String link = linkApi();
    
    final response = await http.get(link+'kecamatan_desa/'+idKec.toString()+'/'+idDesa.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      setState(() {
        kecamatan = jsonDecode(response.body);
        loadKec = false;
      });
    }else{
      showDialog(
        context: context,
        builder: (context) => FadeAnimation(
          0.5,
          new AlertDialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
            title: new Text('Info'),
            content: new Text(response.body),
            actions: <Widget>[
              FadeAnimation(
                1,
                FlatButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(18.0),
                    side: BorderSide(color: Colors.green)),
                  onPressed: () => Navigator.of(context).pop(false),
                  child: Text("Ok", style: TextStyle(color: Colors.green),),
                ),
              ),
            ],
          ),
        ),
      );
    }
  }

  @override
  void initState() {
    ambildataShared();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => keluar(),
      child: Scaffold(
        body: loadingScan ? Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.blue),
              ),
              SizedBox(height: 10,),
              Text("Mengecek data, mohon tunggu...")
            ],
          ),
        ) 
        : Container(
          color: Color(0xFF6EBDFC),
          child: Column(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * 0.12,
                color: Colors.transparent,
                child: Padding(
                  padding: EdgeInsets.only(left:15, right: 15, top: MediaQuery.of(context).size.height * 0.03),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      CircleAvatar(
                        radius: MediaQuery.of(context).size.width * 0.07,
                        backgroundColor: Colors.white,
                        child: CircleAvatar(
                          radius: 25,
                          backgroundImage: AssetImage('assets/gambar/logo.png'),
                          backgroundColor: Colors.white,
                        ),
                      ),
                      AutoSizeText("Social Fund Transfer", style: TextStyle(color: Colors.white, fontSize: MediaQuery.of(context).size.width * 0.06),),
                      Icon(Icons.notifications, color: Colors.white,size:MediaQuery.of(context).size.width * 0.08,)
                    ],
                  ),
                ),
              ),
              Container(
                color: Colors.transparent,
                height: MediaQuery.of(context).size.height * 0.15,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        CircleAvatar(
                          radius: MediaQuery.of(context).size.width * 0.1,
                          backgroundColor: Colors.white,
                          // backgroundImage: AssetImage("assets/gambar/sugihmukti.png"),
                          child: Image.asset("assets/gambar/sugihmukti.png", scale: 4,),
                        ),
                        SizedBox(width: 15,),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('Pencairan', style: TextStyle(color: Colors.white, fontSize: MediaQuery.of(context).size.width * 0.06),),
                            !loadKec ? AutoSizeText('Desa '+kecamatan[0]['kelurahan'], style: TextStyle(color: Colors.white), maxLines: 2, minFontSize: 15,):Text('memuat..', style: TextStyle(color: Colors.white),),
                            !loadKec ? AutoSizeText('Kec. '+kecamatan[0]['kecamatan'], style: TextStyle(color: Colors.white), maxLines: 2, minFontSize: 15,):Text('memuat..', style: TextStyle(color: Colors.white),)
                          ],
                        ),
                      ],
                    ),
                    SizedBox(child: Container(width: 3,height: MediaQuery.of(context).size.height * 0.1,color: Colors.white,),),
                    OutlineButton(
                      child: new Text("Edit Profil", style: TextStyle(color: Colors.white,),),
                      onPressed: (){},
                      borderSide: BorderSide(color: Colors.white),
                      shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0))
                    )
                  ],
                ),
              ),
              Container(
                color: Colors.white,
                child: Column(
                  children: <Widget>[
                    GestureDetector(
                      onTap: (){
                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => PilihKecamatan()));
                      },
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.73,
                        child: Column(
                          children: <Widget>[
                            SizedBox(height: MediaQuery.of(context).size.height * 0.02,),
                            Container(
                              height: MediaQuery.of(context).size.height * 0.12,
                              width: MediaQuery.of(context).size.width * 0.866,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    blurRadius: 10.0,
                                    spreadRadius: 1.0,
                                    offset: Offset(
                                      0.0,
                                      5.0,
                                    ),
                                  )
                                ],
                              ),
                              child: Center(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Image.asset("assets/gambar/calendar.png",scale: 3,),
                                    AutoSizeText("Ubah Titik Pencairan", maxLines: 1, minFontSize: 18, maxFontSize: 20,),
                                    AutoSizeText("Ubah slot desa yang akan dicairkan", maxLines: 1, minFontSize: 12, maxFontSize: 15,),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(height: MediaQuery.of(context).size.height * 0.02,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                GestureDetector(
                                  onTap: (){
                                    scanQr();
                                  },
                                  child: Container(
                                    height: MediaQuery.of(context).size.height * 0.13,
                                    width: MediaQuery.of(context).size.width * 0.4,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(10),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.5),
                                          blurRadius: 10.0,
                                          spreadRadius: 1.0,
                                          offset: Offset(
                                            0.0,
                                            5.0,
                                          ),
                                        )
                                      ],
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, top: MediaQuery.of(context).size.height * 0.01, bottom: MediaQuery.of(context).size.height * 0.01),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                        children: <Widget>[
                                          Image.asset("assets/gambar/scan.png",scale: 5,),
                                          AutoSizeText("Scan QR Code", maxLines: 1, minFontSize: 18, maxFontSize: 20,),
                                          AutoSizeText("Pencairan Dana Bantuan", maxLines: 1, minFontSize: 12, maxFontSize: 15,),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: (){
                                    logout();
                                  },
                                  child: Container(
                                    height: MediaQuery.of(context).size.height * 0.13,
                                    width: MediaQuery.of(context).size.width * 0.4,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(10),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.5),
                                          blurRadius: 10.0,
                                          spreadRadius: 1.0,
                                          offset: Offset(
                                            0.0,
                                            5.0,
                                          ),
                                        )
                                      ],
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, top: MediaQuery.of(context).size.height * 0.01, bottom: MediaQuery.of(context).size.height * 0.01),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                        children: <Widget>[
                                          Image.asset("assets/gambar/logout.png",scale: 5,),
                                          AutoSizeText("Logout", maxLines: 1, minFontSize: 18, maxFontSize: 20,),
                                          AutoSizeText("Keluar Dari Aplikasi", maxLines: 1, minFontSize: 12, maxFontSize: 15,),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        bottomSheet: Container(
          color: Colors.transparent,
          child: Center(child: AutoSizeText('By ASQI DIGITAL INNOVATION - 2020', maxLines: 1, minFontSize: 18, maxFontSize: 20,)),
          height: MediaQuery.of(context).size.height* 0.05,
        ),
      ),
    );
  }

  void scanQr() async {
    var hasilCek;
    var title;
    Color warna;
    bool hasil = await Permission.camera.status.isGranted;
    var status;
    if (!hasil)
      status = await Permission.camera.request();
    
    if(hasil || status == Permission.camera.isGranted){
      String hasilScan = await scanner.scan();
      setState(() {
        loadingScan = true;
      });
      String link = linkApi();
      if (hasilScan != null) {
        final response = await http.get(link+'cek_qr/'+hasilScan.toString()+'/'+idDesaAktif.toString());
        if (response.statusCode == 200 || response.statusCode == 201) {
          hasilCek = await jsonDecode(response.body);
          if(hasilCek['status'] == 0){
            warna = Colors.grey;
            title = "NIK BELUM TERDAFTAR";
          } else if(hasilCek['status'] == 1){
            warna = Colors.green;
            title = "TERKONFIRMASI";
          } else if(hasilCek['status'] == 2){
            warna = Colors.red;
            title = "SUDAH CAIR SEBELUMNYA";
          } else if(hasilCek['status'] == 3){
            warna = Colors.red;
            title = "LOKASI TIDAK SESUAI";
          }

          if (hasilCek['status'] == 1) {
            setState(() {
              loadingScan = false;
            });
            showDialog(
              barrierDismissible: false,
              context: context,
              builder: (context) => FadeAnimation(
                0.5,
                Material(
                  color: Colors.transparent,
                  child: Padding(
                    padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.05),
                    child: Center(
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.9,
                        height: MediaQuery.of(context).size.height * 0.8,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10)
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: Column(
                              children: <Widget>[
                                Container(
                                  height: MediaQuery.of(context).size.height * 0.1,
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                    color: Color(0xFF4CAF50),
                                    borderRadius: BorderRadius.circular(10)
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      AutoSizeText("QR000000001", style: TextStyle(color: Colors.white),maxLines: 1, maxFontSize: 25,minFontSize: 20,),
                                      AutoSizeText(title, style: TextStyle(color: Colors.white),maxLines: 1, maxFontSize: 25, minFontSize: 20,),
                                    ],
                                  )
                                ),
                                SizedBox(height: MediaQuery.of(context).size.height * 0.05,),
                                Padding(
                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            width: 100,  
                                            child: AutoSizeText("Nama", style: TextStyle(color: Colors.black),maxLines: 1, maxFontSize: 20,minFontSize: 17,),
                                          ),
                                          AutoSizeText(" : "+hasilCek['data']['nama'], style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),maxLines: 1, maxFontSize: 20,minFontSize: 17,),
                                        ],
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            width: 100,  
                                            child: AutoSizeText("NIK", style: TextStyle(color: Colors.black),maxLines: 1, maxFontSize: 20,minFontSize: 17,),
                                          ),
                                          AutoSizeText(" : "+hasilCek['data']['nik'], style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),maxLines: 1, maxFontSize: 20,minFontSize: 17,),
                                        ],
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            width: 100,  
                                            child: AutoSizeText("KK", style: TextStyle(color: Colors.black),maxLines: 1, maxFontSize: 20,minFontSize: 17,),
                                          ),
                                          AutoSizeText(" : "+hasilCek['data']['kk'], style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),maxLines: 1, maxFontSize: 20,minFontSize: 17,),
                                        ],
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            width: 100,  
                                            child: AutoSizeText("Desa", style: TextStyle(color: Colors.black),maxLines: 1, maxFontSize: 20,minFontSize: 17,),
                                          ),
                                          AutoSizeText(" : "+hasilCek['data']['nama_kel'], style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),maxLines: 1, maxFontSize: 20,minFontSize: 17,),
                                        ],
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            width: 100,  
                                            child: AutoSizeText("Kecamatan", style: TextStyle(color: Colors.black),maxLines: 1, maxFontSize: 20,minFontSize: 17,),
                                          ),
                                          AutoSizeText(" : "+hasilCek['data']['nama_kec'], style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),maxLines: 1, maxFontSize: 20,minFontSize: 17,),
                                        ],
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            width: 100,  
                                            child: AutoSizeText("Kabupaten", style: TextStyle(color: Colors.black),maxLines: 1, maxFontSize: 20,minFontSize: 17,),
                                          ),
                                          AutoSizeText(" : "+hasilCek['data']['nama_kab'], style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),maxLines: 1, maxFontSize: 20,minFontSize: 17,),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(height: MediaQuery.of(context).size.height * 0.03,),
                                Column(
                                  children: <Widget>[
                                    AutoSizeText("Foto penerima", style: TextStyle(color: Colors.black),maxLines: 1, maxFontSize: 20,minFontSize: 17,),
                                    SizedBox(height: MediaQuery.of(context).size.height * 0.02,),
                                    Image.network('https://image.freepik.com/free-vector/business-people-organization-office-freelance-job-character_40876-1291.jpg', 
                                      height: MediaQuery.of(context).size.height *0.25,
                                    )
                                  ],
                                ),
                                SizedBox(height: MediaQuery.of(context).size.height * 0.03,),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    RaisedButton(
                                      onPressed: () => Navigator.of(context).pop(false),
                                      child: Text('Batal', style: TextStyle(color: Colors.white),),
                                      color: Colors.red,
                                    ),
                                    RaisedButton(
                                      onPressed: (){
                                        Navigator.of(context).pop(false);
                                        cairkan(hasilCek['data']['nik']);
                                      },
                                      child: !loadingCair ? Text('Cairkan', style: TextStyle(color: Colors.white),) 
                                      : CircularProgressIndicator(
                                        valueColor: AlwaysStoppedAnimation(Colors.white),
                                      ),
                                      color: Colors.green,
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ),
            );
          } else {
            setState(() {
              loadingScan = false;
            });
            showDialog(
              barrierDismissible: false,
              context: context,
              builder: (context) => FadeAnimation(
                0.5,
                new AlertDialog(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
                  title: new Text('Info'),
                  content: new Text(title),
                  actions: <Widget>[
                    FadeAnimation(
                      1,
                      new FlatButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(18.0),
                          side: BorderSide(color: warna)),
                        onPressed: () => Navigator.of(context).pop(false),
                        child: Text("Ok", style: TextStyle(color: warna),),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }
        }
      }
    }
  }

  void cairkan(nik) async {
    setState(() {
      loadingCair = true;
    });
    final prefs = await SharedPreferences.getInstance();
    final idUser = jsonDecode(prefs.getString("infoUser")) ?? 0;
    String link = linkApi();
    final response = await http.get(link+'cairkan/'+nik.toString()+'/'+idUser['id'].toString());
      if (response.statusCode == 200 || response.statusCode == 201) {
        final status = jsonDecode(response.body);
        if (status['status'] == 'berhasil') {
          showDialog(
            barrierDismissible: false,
            context: context,
            builder: (context) => FadeAnimation(
              0.5,
              new AlertDialog(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
                title: new Text('Info'),
                content: new Text('Pencairan berhasil'),
                actions: <Widget>[
                  FadeAnimation(
                    1,
                    new FlatButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.green)),
                      onPressed: () => Navigator.of(context).pop(false),
                      child: Text("Ok", style: TextStyle(color: Colors.green),),
                    ),
                  ),
                ],
              ),
            ),
          );
          setState(() {
            loadingCair = false;
          });
        } else {
          showDialog(
            barrierDismissible: false,
            context: context,
            builder: (context) => FadeAnimation(
              0.5,
              new AlertDialog(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
                title: new Text('Info'),
                content: new Text('Pencairan Gagal'),
                actions: <Widget>[
                  FadeAnimation(
                    1,
                    new FlatButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.red)),
                      onPressed: () => Navigator.of(context).pop(false),
                      child: Text("Ok", style: TextStyle(color: Colors.red),),
                    ),
                  ),
                ],
              ),
            ),
          );
          setState(() {
            loadingCair = false;
          });
        }
      }else{
        print(response.body);
        setState(() {
          loadingCair = false;
        });
      }
  }

  void logout(){
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LoginPage()));
  }

  Future<bool> keluar(){
    return showDialog(
      context: context,
      builder: (context) => FadeAnimation(
        0.5,
        new AlertDialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          title: new Text('Konfirmasi'),
          content: new Text('Yakin ingin keluar aplikasi?'),
          actions: <Widget>[
            FadeAnimation(
              1,
              new FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(18.0),
                  side: BorderSide(color: Colors.red)),
                onPressed: () => Navigator.of(context).pop(false),
                child: Text("Tidak", style: TextStyle(color: Colors.red),),
              ),
            ),
            SizedBox(height: 16),
            FadeAnimation(
              1.5,
              new FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(18.0),
                  side: BorderSide(color: Colors.green)),
                onPressed: () => SystemChannels.platform.invokeMethod('SystemNavigator.pop'),
                child: Text("Ya", style: TextStyle(color: Colors.green),),
              ),
            ),
          ],
        ),
      ),
    ) ??
    false;
  }
}