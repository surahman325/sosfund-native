import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:socialfund/animation/fade_animation.dart';
import 'package:socialfund/bjb/pilih_desa.dart';
import '../api.dart';

class PilihKecamatan extends StatefulWidget {
  @override
  _PilihKecamatanState createState() => _PilihKecamatanState();
}

class _PilihKecamatanState extends State<PilihKecamatan> {
  final List<DropdownMenuItem> items = [];
  var datana;
  int selectedValue;
  String link = linkApi();
  bool loadingKec = true;
  bool sudahPilih = false;

  void dataKecamatan() async {
    try {
      final response = await http.get(link+'pilih_kecamatan');
      if (response.statusCode == 200) {
        datana = json.decode(response.body);
        await datana.forEach((data){
          items.add(DropdownMenuItem(
            child: Text(data['nama']),
            value: data['id_kec'],
          ));
        });
        setState(() {
          loadingKec = false;
        });
      } else {
        print(response.body);
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    dataKecamatan();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => keluar(),
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          title: Text('Pilih Kecamatan'),
          backgroundColor: Color(0xFF4FC3F7),
          automaticallyImplyLeading: false
        ),
        body: Container(
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: MediaQuery.of(context).size.height * 0.02,),
              Container(
                color: Colors.transparent,
                height: MediaQuery.of(context).size.height / 4,
                child: Center(child: Image.asset('assets/gambar/map.png'))
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.1,),
              Padding(
                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05, right: MediaQuery.of(context).size.width * 0.05),
                child: !loadingKec ? AutoSizeText("Pilih Kecamatan", maxLines: 1,minFontSize: 20,) : AutoSizeText("Loading...", maxLines: 1,minFontSize: 20,),
              ),
              Padding(
                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05, right: MediaQuery.of(context).size.width * 0.05),
                child: SearchableDropdown.single(
                  items: items,
                  value: selectedValue,
                  hint: "Pilih Kecamatan",
                  searchHint: "Cari Kecamatan",
                  onChanged: (value) {
                    setState(() {
                      selectedValue = value;
                      sudahPilih = true;
                    });
                  },
                  onClear: (){
                    setState(() {
                      sudahPilih = false;
                    });
                  },
                  isExpanded: true,
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.05,),
              Padding(
                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05, right: MediaQuery.of(context).size.width * 0.05),
                child: InkWell(
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height:  MediaQuery.of(context).size.width * 0.12,
                    decoration: BoxDecoration(
                        color: Color(0xFF4FC3F7),
                        borderRadius: BorderRadius.circular(10),
                    ),
                    child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () {
                          sudahPilih ? next(selectedValue) : belumPilih();
                        },
                        child: Center(
                          child: Text("Next",
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: "Poppins-Bold",
                              fontSize: 20,
                              letterSpacing: 1.0
                            )
                          ),
                        ),
                      ),
                    ),
                  )
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void next(value) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setInt("id_kecamatan", value);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => PilihDesa()));
  }

  void belumPilih(){
    showDialog(
      context: context,
      builder: (context) => FadeAnimation(
        0.5,
        new AlertDialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          title: new Text('Info'),
          content: new Text('Silahkan pilih kecamatan terlebih dahulu'),
          actions: <Widget>[
            FadeAnimation(
              1,
              FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(18.0),
                  side: BorderSide(color: Colors.green)),
                onPressed: () => Navigator.of(context).pop(false),
                child: Text("Ok", style: TextStyle(color: Colors.green),),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<bool> keluar(){
    return showDialog(
      context: context,
      builder: (context) => FadeAnimation(
        0.5,
        new AlertDialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          title: new Text('Konfirmasi'),
          content: new Text('Yakin ingin keluar aplikasi?'),
          actions: <Widget>[
            FadeAnimation(
              1,
              new FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(18.0),
                  side: BorderSide(color: Colors.red)),
                onPressed: () => Navigator.of(context).pop(false),
                child: Text("Tidak", style: TextStyle(color: Colors.red),),
              ),
            ),
            SizedBox(height: 16),
            FadeAnimation(
              1.5,
              new FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(18.0),
                  side: BorderSide(color: Colors.green)),
                onPressed: () => SystemChannels.platform.invokeMethod('SystemNavigator.pop'),
                child: Text("Ya", style: TextStyle(color: Colors.green),),
              ),
            ),
          ],
        ),
      ),
    ) ??
    false;
  }
}
