import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:socialfund/animation/fade_animation.dart';
import 'package:socialfund/login/login_page.dart';

class HomeKecamatan extends StatefulWidget {
  @override
  _HomeKecamatanState createState() => _HomeKecamatanState();
}

class _HomeKecamatanState extends State<HomeKecamatan> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => keluar(),
      child: Scaffold(
        body: Container(
          color: Color(0xFF6EBDFC),
          child: Column(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * 0.12,
                color: Colors.transparent,
                child: Padding(
                  padding: EdgeInsets.only(left:15, right: 15, top: MediaQuery.of(context).size.height * 0.03),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      CircleAvatar(
                        radius: MediaQuery.of(context).size.width * 0.07,
                        backgroundColor: Colors.white,
                        child: CircleAvatar(
                          radius: 25,
                          backgroundImage: AssetImage('assets/gambar/logo.png'),
                          backgroundColor: Colors.white,
                        ),
                      ),
                      AutoSizeText("Social Fund Transfer", style: TextStyle(color: Colors.white, fontSize: MediaQuery.of(context).size.width * 0.06),),
                      Icon(Icons.notifications, color: Colors.white,size:MediaQuery.of(context).size.width * 0.08,)
                    ],
                  ),
                ),
              ),
              Container(
                color: Colors.transparent,
                height: MediaQuery.of(context).size.height * 0.15,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        CircleAvatar(
                          radius: MediaQuery.of(context).size.width * 0.1,
                          backgroundColor: Colors.white,
                          // backgroundImage: AssetImage("assets/gambar/sugihmukti.png"),
                          child: Image.asset("assets/gambar/sugihmukti.png", scale: 4,),
                        ),
                        SizedBox(width: 15,),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('Kecamatan', style: TextStyle(color: Colors.white, fontSize: MediaQuery.of(context).size.width * 0.06),),
                            Text('Kec. Cianjur', style: TextStyle(color: Colors.white, fontSize: MediaQuery.of(context).size.width * 0.05),)
                          ],
                        ),
                      ],
                    ),
                    SizedBox(child: Container(width: 3,height: MediaQuery.of(context).size.height * 0.1,color: Colors.white,),),
                    OutlineButton(
                      child: new Text("Edit Profil", style: TextStyle(color: Colors.white,),),
                      onPressed: (){},
                      borderSide: BorderSide(color: Colors.white),
                      shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0))
                    )
                  ],
                ),
              ),
              Container(
                color: Colors.white,
                child: Column(
                  children: <Widget>[
                    Container(
                      color: Colors.white,
                      height: MediaQuery.of(context).size.height * 0.365,
                      child: Stack(
                        children: <Widget>[
                          Center(
                            child: Container(
                              height: MediaQuery.of(context).size.height * 0.08,
                              width: MediaQuery.of(context).size.width * 0.9,
                              decoration: BoxDecoration(
                                color: Color(0xFF6EBDFC),
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    blurRadius: 10.0,
                                    spreadRadius: 1.0,
                                    offset: Offset(
                                      0.0,
                                      5.0,
                                    ),
                                  )
                                ],
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        AutoSizeText('Pencairan', style: TextStyle(color: Colors.white),maxLines: 1, minFontSize: 12, maxFontSize: 20,),
                                        AutoSizeText('135 Juta', style: TextStyle(color: Colors.white),maxLines: 1, minFontSize: 12, maxFontSize: 20,),
                                      ],
                                    ),
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: <Widget>[
                                        AutoSizeText('Total Scan', style: TextStyle(color: Colors.white),maxLines: 1, minFontSize: 12, maxFontSize: 20,),
                                        AutoSizeText('225', style: TextStyle(color: Colors.white),maxLines: 1, minFontSize: 12, maxFontSize: 20,),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Center(
                            child: Container(
                              height: MediaQuery.of(context).size.height * 0.25,
                              color: Colors.transparent,
                              child: Image.asset('assets/gambar/covid.png'),
                            ),
                          ),
                          Positioned(
                            top: MediaQuery.of(context).size.height * 0.27,
                            left: MediaQuery.of(context).size.width * 0.3,
                            child: Container(
                              height: MediaQuery.of(context).size.height * 0.07,
                              width: MediaQuery.of(context).size.width * 0.4,
                              decoration: BoxDecoration(
                                color: Color(0xFF6EBDFC),
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    blurRadius: 10.0,
                                    spreadRadius: 1.0,
                                    offset: Offset(
                                      0.0,
                                      5.0,
                                    ),
                                  )
                                ],
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  AutoSizeText('Total Penerima', style: TextStyle(color: Colors.white),maxLines: 1, minFontSize: 12, maxFontSize: 20,),
                                  AutoSizeText('135 Orang', style: TextStyle(color: Colors.white),maxLines: 1, minFontSize: 12, maxFontSize: 20,),
                                ],
                              ),
                            ),
                          )
                        ],
                      )
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.365,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            height: MediaQuery.of(context).size.height * 0.12,
                            width: MediaQuery.of(context).size.width * 0.4,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  blurRadius: 10.0,
                                  spreadRadius: 1.0,
                                  offset: Offset(
                                    0.0,
                                    5.0,
                                  ),
                                )
                              ],
                            ),
                            child: Padding(
                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, top: MediaQuery.of(context).size.height * 0.01, bottom: MediaQuery.of(context).size.height * 0.01),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  Image.asset("assets/gambar/research.png",scale: 5,),
                                  AutoSizeText("Lihat Data Penerima", maxLines: 1, minFontSize: 18, maxFontSize: 20,),
                                  AutoSizeText("Pencairan Dana Bantuan", maxLines: 1, minFontSize: 12, maxFontSize: 15,),
                                ],
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: (){
                              logout();
                            },
                            child: Container(
                              height: MediaQuery.of(context).size.height * 0.12,
                              width: MediaQuery.of(context).size.width * 0.4,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    blurRadius: 10.0,
                                    spreadRadius: 1.0,
                                    offset: Offset(
                                      0.0,
                                      5.0,
                                    ),
                                  )
                                ],
                              ),
                              child: Padding(
                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, top: MediaQuery.of(context).size.height * 0.01, bottom: MediaQuery.of(context).size.height * 0.01),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Image.asset("assets/gambar/logout.png",scale: 5,),
                                    AutoSizeText("Logout", maxLines: 1, minFontSize: 18, maxFontSize: 20,),
                                    AutoSizeText("Keluar Dari Aplikasi", maxLines: 1, minFontSize: 12, maxFontSize: 15,),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void logout(){
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LoginPage()));
  }

  Future<bool> keluar(){
    return showDialog(
      context: context,
      builder: (context) => FadeAnimation(
        0.5,
        new AlertDialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          title: new Text('Konfirmasi'),
          content: new Text('Yakin ingin keluar aplikasi?'),
          actions: <Widget>[
            FadeAnimation(
              1,
              new FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(18.0),
                  side: BorderSide(color: Colors.red)),
                onPressed: () => Navigator.of(context).pop(false),
                child: Text("Tidak", style: TextStyle(color: Colors.red),),
              ),
            ),
            SizedBox(height: 16),
            FadeAnimation(
              1.5,
              new FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(18.0),
                  side: BorderSide(color: Colors.green)),
                onPressed: () => SystemChannels.platform.invokeMethod('SystemNavigator.pop'),
                child: Text("Ya", style: TextStyle(color: Colors.green),),
              ),
            ),
          ],
        ),
      ),
    ) ??
    false;
  }
}